/*******************************************************************************
 * Copyright 2016, Uburu by Anass TAHA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.xusa.core.game;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.xusa.core.game.objects.Grid;


public final class GameLauncher {

    public static void main(String[] args) {
        System.out.println("launchng 2048 game ...");
        
        JFrame game = new JFrame();
        game.setTitle("2048 Game [BOT]");
        game.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        game.setSize(340, 450);
        game.setResizable(false);

        game.add(new Grid());

        game.setLocationRelativeTo(null);
        game.setVisible(true);
    }

}
