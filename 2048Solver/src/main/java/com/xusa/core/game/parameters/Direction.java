package com.xusa.core.game.parameters;

public enum Direction {
    LEFT, RIGHT, UP, DOWN;
}
